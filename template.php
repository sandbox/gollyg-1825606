<?php

/**
 * @file
 * Provides hook and theme overrides to implement the bootstrappy theme.
 */


/*********************
 HOOK IMPLEMENTATIONS
*********************/

/**
 * Implementation of hook_form_alter().
 *
 * This is how we control the style of a form. Default will be form-normal.
 */
function bootstrappy_form_alter(&$form, &$form_state, $form_id) {
  if (empty($form['#bootstrappy_form_style'])) {
    //form does not has not been processed
    switch ($form_id) {
      case 'contact_site_form':
        bootstrappy_set_form_style($form, 'horizontal');
        break;

      default:
        bootstrappy_set_form_style($form, 'normal');
        break;
    }
  }
}

/**
 * Implementation of hook_form_FORM_ID_alter().
 *
 * Sets up the search box to use bootstrap html structure.
 */
function bootstrappy_form_search_block_form_alter(&$form, &$form_state) {
  $form['#attributes']['class'][] = 'form-search';
  $form['#bootstrappy_form_style'] = 'normal';
  $form['search_block_form']['#attributes']['class'][] = 'input-medium search-query';
  $form['submit'] = $form['actions']['submit'];
  unset($form['actions']);
  bootstrappy_set_form_style($form, 'normal');
}
/*********************
 PREPROCESS FUNCTIONS
*********************/

/**
 * Implementation of hook_preprocess_html().
 *
 */
function bootstrappy_preprocess_html(&$variables) {
  // are we responsive?
  $responsive = theme_get_setting('bootstrappy_responsive');
  if ($responsive) {
    // add viewport meta, to control page zoom on mobile devices that support it
    $responsive_meta = '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
    $element = array(
      '#type' => 'markup',
      '#markup' => $responsive_meta,
    );
    drupal_add_html_head($element, 'bootstrappy_responsive');

    //add bootstrap responsive stylesheet
    $path = drupal_get_path('theme', 'bootstrappy');
    drupal_add_css($path . '/assets/css/responsive.css', array('group' => CSS_THEME));
  }
}

/**
 * Implementation of hook_preprocess_page().
 *
 */
function bootstrappy_process_page(&$variables) {
  //add enabled js to footer
  $enabled_bootstrap_js = theme_get_setting('bootstrappy_enabled_js');
  $footer_scripts = array();
  $path = drupal_get_path('theme', 'bootstrappy');

  if(!empty($enabled_bootstrap_js)) {
    foreach ($enabled_bootstrap_js as $script => $enabled) {
      if ($enabled) {
        $filepath = $path . '/assets/js/' . $script . '.js';
          drupal_add_js($filepath, array('scope' => 'footer'));
      }
    }
  }

  //add correct column classes based upon sidebar status
  if ($variables['page']['sidebar_first'] && $variables['page']['sidebar_second']) {
    // both sidebars enabled
    $bootstrappy_classes = array(
      'sidebar_first' => 'span3',
      'sidebar_second' => 'span3',
      'content' => 'span6',
    );
  }
  elseif ($variables['page']['sidebar_first']) {
    // first sidebar only
    $bootstrappy_classes = array(
      'sidebar_first' => 'span4',
      'content' => 'span8',
    );
  }
  elseif ($variables['page']['sidebar_second']) {
    // first sidebar only
    $bootstrappy_classes = array(
      'sidebar_first' => 'span4',
      'content' => 'span8',
    );
  }
  else {
    //just the content
    $bootstrappy_classes = array('content' => 'span12');
  }

  $variables['bootstrappy']['classes'] = $bootstrappy_classes;
}

/**
 * Implementation of hook_preprocess_region().
 */
function bootstrappy_preprocess_region(&$variables) {
  switch ($variables['region']) {
    case 'highlighted':
      //highglighted region in bootstrappy is the hero-unit
      $variables['classes_array'][] = 'hero-unit';
      break;
    case 'navigation':
      // forcibly overwrite the classes on the nav region
      $variables['classes_array'] = array('navbar-inner');
      break;
    default:
      break;
  }
}

/**
 * Implementation of hook_proprocess_block().
 */
function bootstrappy_preprocess_block(&$variables) {
  //we clean up the markup, or make it more semantic, for some known blocks
  //sub themes should add additional blocks in their temeplate.php files.
  $module_delta = $variables['block']->module . '-' . $variables['block']->delta;
  switch ($module_delta) {
    // section wrapper
    case 'comment-recent':
    case 'system-main':
      bootstrappy_clean_block_html($variables, 'section');
      break;
    // clean form - no wrapper
    case 'search-form':
      bootstrappy_clean_block_html($variables);
    default:
      break;
  }
}

/**
 * Implementation of hook_preprocess_table().
 */
function bootstrappy_preprocess_table(&$variables) {
  //add bootstrap table class
  $variables['attributes']['class'][] = 'table';
  $variables['attributes']['class'][] = 'table-striped';
  $variables['attributes']['class'][] = 'table-hover';
}

/**
 * Implementation of hook_preprocess_button().
 */
function bootstrappy_preprocess_button(&$variables) {
  $variables['element']['#attributes']['class'][] = 'btn';
  if ($variables['element']['#type'] == 'submit') {
    $variables['element']['#attributes']['class'][] = 'btn-primary';
  }
}

/************************
 CUSTOM UTILITY FUNCTIONS
************************/

/**
 * Sets a bootstrappy style attibute on forms.
 *
 * This function will set the #boostrappy_form_style on the element and all its
 * children. This function is called recursivley on the parent element.
 *
 * @param $element
 * The element to be modified
 * @param $property
 * The bootstrappy form style to be added
 * @todo  Generalise this more, so you can pass in a propery name and a
 * property value, if there turns out to be a use case.
 */
function bootstrappy_set_form_style(&$element, $property) {

  if (is_array($element)) {
    if ($children = element_children($element)) {
      foreach ($children as $child) {
        bootstrappy_set_form_style($element[$child], $property);
      }
    }
    if (!empty($element['#type'])) {
      if (!empty($element['#description'])) {
        $element['#attributes']['title'] = check_plain($element['#description']);
        unset($element['#description']);
      }
      $element['#bootstrappy_form_style'] = $property;
    }
  }
}

/**
 * Sets the template and template variables to clean up the block markup or
 * make it more semantic.
 *
 * @param $element
 * The block element to be themed
 * @param  $wrapper
 * The tagname of the wrapper element (eg section), or NULL if no wrapper is to
 * be applied.
 * @param  $heading_element
 * The heading level to be used for the block heading.
 */
function bootstrappy_clean_block_html(&$element, $wrapper = NULL, $heading_element = 'h3') {
  $element['bootstrappy_settings']['block']['wrapper'] = $wrapper;
  $element['bootstrappy_settings']['block']['heading_element'] = $heading_element;
  $element['theme_hook_suggestions'][] = 'block__bootstrappy__block';
}


/************************
 THEME FUNCTION OVERRIDES
************************/

/**
 * Override theme_menu_tree().
 *
 * Bootstrap uses a class name of 'nav' on the navigation list.
 *
 * @ingroup themeable
 */
function bootstrappy_menu_tree($variables) {
  return '<ul class="nav">' . $variables['tree'] . '</ul>';
}

/**
 * Override theme_menu_link.
 *
 * Bootstrap uses an active class on the li element, whereas Drupal places it
 * on the a element.
 *
 * @ingroup themeable
 */
function bootstrappy_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  // Append active class to li element (where bootstrap likes it).
  $path = $element['#href'];
  if (($path == $_GET['q'] || ($path == '<front>' && drupal_is_front_page())) &&
      (empty($options['language']) || $options['language']->language == $language_url->language)) {
    $element['#attributes']['class'][] = 'active';
  }

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);


  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Override theme_breadcrumb().
 *
 * @ingroup themeable
 */
function bootstrappy_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    $breadcrumb[] = drupal_get_title();
    $output = '<div class="breadcrumb">';
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output .= '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= implode(' > ', $breadcrumb);
    $output .= '</div>';
    return $output;
  }
}

/**
 * Override theme_mark().
 *
 * Adds leavel classes to alter the display of thise items.
 *
 * @ingroup themeable
 */
function bootstrappy_mark($variables) {
  $type = $variables['type'];
  global $user;
  if ($user->uid) {
    if ($type == MARK_NEW) {
      return ' <span class="label label-success">' . t('new') . '</span>';
    }
    elseif ($type == MARK_UPDATED) {
      return ' <span class="label label-info">' . t('updated') . '</span>';
    }
  }
}

/**
 * Override theme_item_list().
 *
 * Change the wrapper div to be conditional, based upon the presence of a title.
 * The reduces the amount of redundant markup.
 *
 * @ingroup themeable
 */
function bootstrappy_item_list($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];
  $output = '';

  // Only output the list container and title, if there are any list items.
  // Check to see whether the block title exists before adding a header.
  // Empty headers are not semantic and present accessibility challenges.
  if (isset($title) && $title !== '') {
    $output = '<div class="item-list">';
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    foreach ($items as $i => $item) {
      $attributes = array();
      $children = array();
      $data = '';
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= bootstrappy_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 0) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items - 1) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }
  if (isset($title) && $title !== '') {
    $output .= '</div>';
  }

  return $output;
}

/**
 * Overrides theme_more_help_link().
 *
 * Adds a button class to the link to alter its appearance.
 *
 * @ingroup themeable
 */
function bootstrappy_more_help_link($variables) {
  return '<div class="more-help-link">' . l(t('More help'), $variables['url'], array('attributes' => array('class' => 'btn btn-info'))) . '</div>';
}

/**
 * Override theme_more_link().
 *
 * @ingroup themeable
 */
function bootstrappy_more_link($variables) {
  return '<div class="more-link">' . l(t('More'), $variables['url'], array('attributes' => array('title' => $variables['title']))) . '</div>';
}

/**
 * Override theme_menu_local_tasks().
 *
 * Uses bootsrap theme stypes for local task navigation.
 *
 * @ingroup themeable
 *
 * @todo Add theme setting to control the style used.
 */
function bootstrappy_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="nav nav-tabs">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="nav nav-pills">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Override default image theme function.
 *
 * This override allows us to disable the height attribute on images when
 * the responsive grid is being used.
 *
 * @ingroup themeable
 */
function bootstrappy_image($variables) {
  $attributes = $variables['attributes'];
  $attributes['src'] = file_create_url($variables['path']);
  $attributes['class'][] = 'img-polaroid';

  foreach (array('width', 'height', 'alt', 'title') as $key) {

    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }

  //unset the height for responsive themes
  $responsive = theme_get_setting('bootstrappy_responsive');
  if ($responsive) {
    if (!empty($attributes['height'])) {
      unset($attributes['height']);
    }
  }

  return '<img' . drupal_attributes($attributes) . ' />';
}


/**
 * Override theme_form().
 *
 * Add bootstrap form class and remove redundant internal div wrapper, as it is
 * not required in HMTL5.
 *
 * @ingroup themeable
 */
function bootstrappy_form($variables) {
  $element = $variables['element'];

  if (isset($element['#action'])) {
    $element['#attributes']['action'] = drupal_strip_dangerous_protocols($element['#action']);
  }
  element_set_attributes($element, array('method', 'id'));
  if (empty($element['#attributes']['accept-charset'])) {
    $element['#attributes']['accept-charset'] = "UTF-8";
  }
  // Add our bootstrappy style
  if (!empty($element['#bootstrappy_form_style'])) {
    $bootstrappy_class = 'form-' . $element['#bootstrappy_form_style'];
    $element['#attributes']['class'][] = $bootstrappy_class;
  }

  return '<form ' . drupal_attributes($element['#attributes']) . '>' . $element['#children'] . '</form>';
}

/**
 * Override theme_form_element().
 *
 * Add in support for bootstrap inline and horizontal form styles, via the
 * #bootstrappy_form_style variable.
 *
 * @ingroup themeable
 *
 * @todo  What to do with the rendering of the attributes array?
 */
function bootstrappy_form_element($variables) {
  $element = &$variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }


  $output = '';
  // $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  //change the rendering based upon the bootstrap form style
  if (empty($element['#bootstrappy_form_style'])) {
    $element['#bootstrappy_form_style'] = 'normal';
  }

  switch ($element['#bootstrappy_form_style']) {
    case 'horizontal':
      $control_group_wrapper_start = '<div class="control-group">';
      $control_group_wrapper_end = '</div>';
      $control_wrapper_start = '<div class="controls">';
      $control_wrapper_end = '</div>';

      //horizontal forms always use the before style - force it to be so
      $element['#title_display'] = 'before';
      break;

    case 'inline':
      if (!empty($element['#description'])) {
        unset($element['#description']);
      }
    case 'normal':
    default:
      $control_group_wrapper_start = '';
      $control_group_wrapper_end = '';
      $control_wrapper_start = '';
      $control_wrapper_end = '';
      break;
  }

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output = $control_group_wrapper_start;
      $output .= theme('form_element_label', $variables);
      $output .= $control_wrapper_start . $prefix . $element['#children'] . $control_wrapper_end . $suffix . $control_group_wrapper_end . "\n";
      break;

    case 'after':
      $output .= $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element['#description'])) {
    $output .= '<span class="help-block">' . $element['#description'] . "</span>\n";
  }
  return $output;
}

/**
 * Override of theme_form_element_label().
 *
 * Form labels vary based upon the bootsrap form style.
 *
 * @ingroup themeable
 */
function bootstrappy_form_element_label($variables) {
  $element = $variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'][] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'][] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  if ($element['#bootstrappy_form_style'] == 'horizontal') {
    $attributes['class'][] = 'control-label';
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n";
}

/**
 * Override of theme_links().
 *
 * Add in custom bootstrap classes for links display.
 *
 * @ingroup themeable
 *
 * @todo  Create a theme setting to control the styles.
 * @todo  Can this be done more efficiently through a preprocess function?
 */
function bootstrappy_links($variables) {
  $links = $variables['links'];
  $attributes = $variables['attributes'];
  $heading = $variables['heading'];
  global $language_url;
  $output = '';

  if (count($links) > 0) {
    $output = '';

    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = array(
          'text' => $heading,
          // Set the default level of the heading.
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $output .= '<ul' . drupal_attributes($attributes) . '>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = array($key);

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class[] = 'first';
      }
      if ($i == $num_links) {
        $class[] = 'last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
           && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class[] = 'active';
      }
      $output .= '<li' . drupal_attributes(array('class' => $class)) . '>';

      if (isset($link['href'])) {
        $link['attributes']['class'][] = 'btn';
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span ' . $span_attributes . '>' . $link['title'] . '</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}
