<?php

/**
 * @file Provides custom settings for the Boostrappy theme.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function bootstrappy_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['bootstrap_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bootstrap settings'),
  );
  $form['bootstrap_wrapper']['bootstrappy_grid_type'] = array(
    '#type' => 'radios',
    '#title' => t('Grid type'),
    '#options' => array(
      'fixed' => t('Fixed'),
      'fluid' => t('Fluid'),
    ),
    '#default_value' => theme_get_setting('bootstrappy_grid_type'),
  );
  $form['bootstrap_wrapper']['bootstrappy_responsive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable responsive theme elements'),
    '#default_value' => theme_get_setting('bootstrappy_responsive'),
  );
  $form['bootstrap_wrapper']['bootstrappy_enabled_js'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled scripts'),
    '#description' => t('Find out more about the options at <a href="http://twitter.github.com/bootstrap/javascript.html">Bootstrap github overview</a>'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
    '#options' => bootstrappy_js_options(),
    '#default_value' => theme_get_setting('bootstrappy_enabled_js'),
  );

}

/**
 * Returns an array of potential javascript files used by Boostrap.
 */
function bootstrappy_js_options() {
  return array(
    'jquery' => t('jQuery'),
    'bootstrap-transtion' => t('Transtions'),
    'bootstrap-alert' => t('Alerts'),
    'bootstrap-modal' => t('Modals'),
    'bootstrap-dropdown' => t('Dropdowns'),
    'bootstrap-spy' => t('Scrollspy'),
    'bootstrap-tab' => t('Tabs'),
    'bootstrap-tooltip' => t('Tooltips'),
    'bootstrap-popover' => t('Popovers'),
    'bootstrap-button' => t('Buttons'),
    'bootstrap-collapse' => t('Collapse'),
    'bootstrap-carousel' => t('Carousel'),
    'bootstrap-typeahead' => t('Type ahead'),
  );
}
