<?php
/**
 * @file
 * Display Suite bootstrap_article template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 *
 * Regions:
 *
 * - $content: Rendered content for the "Content" region.
 * - $content_classes: String of classes that can be used to style the "Content" region.
 */
?>
<article class="<?php print $classes; ?> <?php print $article_content_classes ?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <?php if ($article_content): ?>
      <?php print $article_content; ?>
  <?php endif; ?>

</article>
