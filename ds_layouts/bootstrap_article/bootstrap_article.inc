<?php

/**
 * @file
 * Display Suite bootstrap_article configuration.
 */

function ds_bootstrap_article() {
  return array(
    'label' => t('bootstrap_article'),
    'regions' => array(
      'article_content' => t('Content'),
    ),
    // Uncomment if you want to include a CSS file for this layout (bootstrap_article.css)
    // 'css' => TRUE,
    // Uncomment if this is a template for a node form.
    // 'form' => TRUE,
  );
}
