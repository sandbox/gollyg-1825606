<?php
/**
 * @file
 * Display Suite Bootstrap 2 column with hero template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 *
 * Regions:
 *
 * - $hero: Rendered content for the "hero" region.
 * - $hero_classes: String of classes that can be used to style the "hero" region.
 *
 * - $left: Rendered content for the "left" region.
 * - $left_classes: String of classes that can be used to style the "left" region.
 *
 * - $right: Rendered content for the "right" region.
 * - $right_classes: String of classes that can be used to style the "right" region.
 *
 * - $footer: Rendered content for the "footer" region.
 * - $footer_classes: String of classes that can be used to style the "footer" region.
 */
?>
<article<?php if($classes): ?> class="<?php print $classes ?>"><?php endif ?>
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <?php if ($hero): ?>
    <div class="row">
      <div class="ds-hero<?php print $hero_classes; ?>">
        <?php print $hero; ?>
      </div>
    </div>
  <?php endif; ?>

  <div class="row middle">
    <?php if ($left): ?>
      <div class="ds-left<?php print $left_classes; ?>">
        <?php print $left; ?>
      </div>
    <?php endif; ?>

    <?php if ($right): ?>
      <div class="ds-right<?php print $right_classes; ?>">
        <?php print $right; ?>
      </div>
    <?php endif; ?>
  </div>

  <?php if ($footer): ?>
    <div class="row">
      <div class="ds-footer<?php print $footer_classes; ?>">
        <?php print $footer; ?>
      </div>
    </div>
  <?php endif; ?>
</article>
