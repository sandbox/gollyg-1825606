<?php

/**
 * @file
 * Display Suite Bootstrap 2 column with hero configuration.
 */

function ds_bootstrap_2_column_with_hero() {
  return array(
    'label' => t('Bootstrap 2 column with hero'),
    'regions' => array(
      'hero' => t('hero'),
      'left' => t('left'),
      'right' => t('right'),
      'footer' => t('footer'),
    ),
    // Uncomment if you want to include a CSS file for this layout (bootstrap_2_column_with_hero.css)
    // 'css' => TRUE,
    // Uncomment if this is a template for a node form.
    // 'form' => TRUE,
  );
}
