<?php
/**
 * @file
 * Display Suite Bootstrap 2 column with hero template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 *
 * Regions:
 *
 * - $hero: Rendered content for the "hero" region.
 * - $hero_classes: String of classes that can be used to style the "hero" region.
 *
 * - $left: Rendered content for the "left" region.
 * - $left_classes: String of classes that can be used to style the "left" region.
 *
 * - $right: Rendered content for the "right" region.
 * - $right_classes: String of classes that can be used to style the "right" region.
 *
 * - $footer: Rendered content for the "footer" region.
 * - $footer_classes: String of classes that can be used to style the "footer" region.
 */
?>
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <?php if ($top): ?>
    <div class="ds-top<?php print $top_classes; ?>">
      <?php print $top; ?>
    </div>
  <?php endif; ?>

  <?php if ($middle): ?>
    <div class="ds-middle<?php print $middle_classes; ?>">
      <?php print $middle; ?>
    </div>
  <?php endif; ?>

  <?php if ($bottom): ?>
    <div class="ds-bottom<?php print $bottom_classes; ?>">
      <?php print $bottom; ?>
    </div>
  <?php endif; ?>
