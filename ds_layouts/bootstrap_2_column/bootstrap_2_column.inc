<?php

/**
 * @file
 * Display Suite bootstrap 2 column configuration.
 */

function ds_bootstrap_2_column() {
  return array(
    'label' => t('bootstrap 2 column'),
    'regions' => array(
      'left' => t('left'),
      'right' => t('right'),
    ),
    // Uncomment if you want to include a CSS file for this layout (bootstrap_2_column.css)
    // 'css' => TRUE,
    // Uncomment if this is a template for a node form.
    // 'form' => TRUE,
  );
}
