<?php
/**
 * @file Returns the markup for the stands page HMTL.
 */
?>

<div id="page-container" class="container">

  <header id="header" class="jumbotron subhead">
    <h1><a href="/"><?php print $site_name ?></a></h1>
    <?php if($site_slogan): ?>
      <p class="lead"><?php print $site_slogan ?></p>
    <?php endif ?>

    <?php if ($page['header']): ?>
      <?php print render($page['header']) ?>
    <?php endif ?>
  </header>

    <?php if ($page['navigation']): ?>
      <nav class="navbar clearfix">
        <?php print render($page['navigation']) ?>
      </nav>
    <?php endif ?>

  <?php if ($page['highlighted']) : ?>
    <section id="highlighted">
      <?php print render($page['highlighted']) ?>
    </section>
  <?php endif ?>

  <?php if ($breadcrumb) : ?>
    <?php print ($breadcrumb) ?>
  <?php endif ?>

  <div class="row">
    <?php if ($page['sidebar_first']) : ?>
      <aside class="<?php print $bootstrappy['classes']['sidebar_first'] ?>">
        <?php print render($page['sidebar_first']) ?>
      </aside>
    <?php endif ?>

    <div class="<?php print $bootstrappy['classes']['content'] ?>">

      <?php if ($messages) : ?>
        <?php print ($messages) ?>
      <?php endif ?>

      <?php if ($tabs) : ?>
        <?php print render($tabs) ?>
      <?php endif ?>

      <?php if ($page['help']): ?>
        <?php render($page['help']) ?>
      <?php endif ?>

      <?php if ($page['content']): ?>
        <section id="content">
          <?php if ($title && !$is_front) : ?>
            <header>
              <h2 class="row"><span class="span12"><?php print ($title) ?></span></h2>
            </header>
          <?php endif ?>
          <?php print render($page['content']) ?>
        </section>
      <?php endif ?>
    </div>

    <?php if ($page['sidebar_second']) : ?>
      <aside class="<?php print $bootstrappy['classes']['sidebar_second'] ?>">
        <?php print render($page['sidebar_second']) ?>
      </aside>
    <?php endif ?>

  </div>

  <?php if ($page['footer']): ?>
    <footer>
      <?php print render($page['footer']) ?>
    </footer>
  <?php endif; ?>

</div>
