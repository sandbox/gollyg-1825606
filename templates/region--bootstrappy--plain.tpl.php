<?php
/**
 * @file Provides clean markup for regions
 *
 * @todo  Generlise the to allow custom markup based upon variable flags.
 */
?>

<?php if ($content): ?>
<?php print $content ?>
<?php endif ?>
